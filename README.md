[[_TOC_]]

# CFGS DAW M08
## Sobre el projecte

Aquest projecte és la meva primera demo en gitlab

## Contribuidors
**Pol Ruiz** - ***@ruto-rtp***

## Contacte
Eloi Figueroa Dorado - 22efigueroa@ibadia.cat
<br/>
Enllaç del projecte:
[https://gitlab.com/22efigueroa/m08.git](https://gitlab.com/22efigueroa/m08.git)
